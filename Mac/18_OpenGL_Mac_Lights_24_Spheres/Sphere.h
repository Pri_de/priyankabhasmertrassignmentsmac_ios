//
//  Sphere.h
//  
//
//  Created on 03/08/18.
//

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

#define iSlices 30
#define iStacks 30
#define fRadius 0.75
#define numIndices (iSlices * iStacks * 6)
#define iNumIndices (numIndices/3)
GLint elements[iNumIndices * 3 * 2];
GLfloat verts[iNumIndices * 3 * 4];
GLfloat norms[iNumIndices * 2 * 4];
GLfloat texCoords[iNumIndices * 3 * 2];
GLint numElements=0;
GLint maxElements=0;
GLint numVertices=0;
GLuint vbo_position=0;
GLuint vbo_normal=0;
GLuint vbo_texture=0;
GLuint vbo_index=0;
GLuint vao=0;

void prepareToDraw(void)
{
    // vao
    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);
    
    // vbo for position
    glGenBuffers(1, &vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // vbo for normals
    glGenBuffers(1, &vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(norms), norms, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // vbo for texture
    glGenBuffers(1, & vbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords), texCoords, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    
    // vbo for index
    glGenBuffers(1, &vbo_index);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    // after sending data to GPU, now we can free our arrays
    //cleanupMeshData();
}

void normalizeVector(float v[3])
{
    // code
    
    // square the vector length
    float squaredVectorLength=(v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
    
    // get square root of above 'squared vector length'
    float squareRootOfSquaredVectorLength=sqrt(squaredVectorLength);
    
    // scale the vector with 1/squareRootOfSquaredVectorLength
    v[0] = v[0] * 1.0/squareRootOfSquaredVectorLength;
    v[1] = v[1] * 1.0/squareRootOfSquaredVectorLength;
    v[2] = v[2] * 1.0/squareRootOfSquaredVectorLength;
    
    
}

char isFoundIdentical (float val1, float val2, float diff)
{
    // code
    if(abs(val1 - val2) < diff)
        return(1);
    else
        return(0);
    
    
}
void addTriangle(float single_vertex[][3], float single_normal[][3] , float single_texture[][2])
{
    //variable declarations
    const float diff = 0.00001;
    int i, j;
    // code
    // normals should be of unit length
    normalizeVector(single_normal[0]);
    normalizeVector(single_normal[1]);
    normalizeVector(single_normal[2]);
    
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
            if (isFoundIdentical(verts[j * 3], single_vertex[i][0], diff) &&
                isFoundIdentical(verts[(j * 3) + 1], single_vertex[i][1], diff) &&
                isFoundIdentical(verts[(j * 3) + 2], single_vertex[i][2], diff) &&
                
                isFoundIdentical(norms[j * 3], single_normal[i][0], diff) &&
                isFoundIdentical(norms[(j * 3) + 1], single_normal[i][1], diff) &&
                isFoundIdentical(norms[(j * 3) + 2], single_normal[i][2], diff) &&
                
                isFoundIdentical(texCoords[j * 2], single_texture[i][0], diff) &&
                isFoundIdentical(texCoords[(j * 2) + 1], single_texture[i][1], diff))
            {
                elements[numElements] = j;
                numElements++;
                break;
            }
        }
        
        //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
        if (j == numVertices && numVertices < maxElements && numElements < maxElements)
        {
            verts[numVertices * 3] = single_vertex[i][0];
            verts[(numVertices * 3) + 1] = single_vertex[i][1];
            verts[(numVertices * 3) + 2] = single_vertex[i][2];
            
            norms[numVertices * 3] = single_normal[i][0];
            norms[(numVertices * 3) + 1] = single_normal[i][1];
            norms[(numVertices * 3) + 2] = single_normal[i][2];
            
            texCoords[numVertices * 2] = single_texture[i][0];
            texCoords[(numVertices * 2) + 1] = single_texture[i][1];
            
            elements[numElements] = numVertices; //adding the index to the end of the list of elements/indices
            numElements++; //incrementing the 'end' of the list
            numVertices++; //incrementing coun of vertices
        }
    }
    
    
}
void DrawSphere() //(sphereMesh, fRadius, iSlices, iStacks) 2 30 30
{
    const GLfloat VDG_PI = 3.14159265358979323846;
    
    GLfloat drho, dtheta, ds,dt,t,s;
    GLint i,j;
    drho =(VDG_PI) /GLfloat(iStacks);
    dtheta = 2.0 *(VDG_PI) /GLfloat(iSlices);
    ds = 1.0 /GLfloat(iSlices);
    dt = 1.0 /GLfloat(iStacks);
    t = 1.0;
    s = 0.0;
    i=0;
    j=0;
    
    maxElements = numIndices;
    numElements = 0;
    numVertices = 0;
    
    //======================================================
    for (i = 0; i < iStacks; i++)
    {
        float rho =GLfloat(i * drho);
        float srho = sin(rho);
        float crho = cos(rho);
        float srhodrho = sin(rho + drho);
        float crhodrho = cos(rho + drho);
        
        // Many sources of OpenGL sphere drawing code uses a triangle fan
        // for the caps of the sphere. This however introduces texturing
        // artifacts at the poles on some OpenGL implementations
        s = 0.0;
        
        // initialization of three 2-D arrays, two are 4 x 3 and one is 4 x 2
        float vertex[4][3];
        float normal[4][3];
        float texture[4][2];
        
        for ( j = 0; j < iSlices; j++)
        {
            float theta;// = if((j == iSlices) ? 0.0 : j * dtheta);
            if(j == iSlices)
                theta = 0.0;
            else
                theta = j * dtheta;
            float stheta =-sin(theta);
            float ctheta =cos(theta);
            
            float x = stheta * srho;
            float y = ctheta * srho;
            float z = crho;
            
            texture[0][0] = s;
            texture[0][1] = t;
            normal[0][0] = x;
            normal[0][1] = y;
            normal[0][2] = z;
            vertex[0][0] = x * fRadius;
            vertex[0][1] = y * fRadius;
            vertex[0][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[1][0] = s;
            texture[1][1] = t - dt;
            normal[1][0] = x;
            normal[1][1] = y;
            normal[1][2] = z;
            vertex[1][0] = x * fRadius;
            vertex[1][1] = y * fRadius;
            vertex[1][2] = z * fRadius;
            
            //theta = if(((j+1) == iSlices) ? 0.0 : (j+1) * dtheta);
            if((j+1) == iSlices)
                theta = 0.0;
            else
                theta = (j+1) * dtheta;
            stheta =-sin(theta);
            ctheta =cos(theta);
            
            
            x = stheta * srho;
            y = ctheta * srho;
            z = crho;
            
            s += ds;
            texture[2][0] = s;
            texture[2][1] = t;
            normal[2][0] = x;
            normal[2][1] = y;
            normal[2][2] = z;
            vertex[2][0] = x * fRadius;
            vertex[2][1] = y * fRadius;
            vertex[2][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[3][0] = s;
            texture[3][1] = t - dt;
            normal[3][0] = x;
            normal[3][1] = y;
            normal[3][2] = z;
            vertex[3][0] = x * fRadius;
            vertex[3][1] = y * fRadius;
            vertex[3][2] = z * fRadius;
            
            
            //===============================================
            addTriangle(vertex, normal, texture);
            
            //===============================================
            // Rearrange for next triangle
            vertex[0][0]=vertex[1][0];
            vertex[0][1]=vertex[1][1];
            vertex[0][2]=vertex[1][2];
            normal[0][0]=normal[1][0];
            normal[0][1]=normal[1][1];
            normal[0][2]=normal[1][2];
            texture[0][0]=texture[1][0];
            texture[0][1]=texture[1][1];
            
            vertex[1][0]=vertex[3][0];
            vertex[1][1]=vertex[3][1];
            vertex[1][2]=vertex[3][2];
            normal[1][0]=normal[3][0];
            normal[1][1]=normal[3][1];
            normal[1][2]=normal[3][2];
            texture[1][0]=texture[3][0];
            texture[1][1]=texture[3][1];
            
            addTriangle(vertex, normal, texture);
        }
        t -= dt;
    }
    
    prepareToDraw();
    
}

